### Cách build project với libtorch 
1. Download libtorch ví dụ version 1.11.0+cpu tại [link](https://download.pytorch.org/libtorch/cpu/libtorch-shared-with-deps-1.11.0%2Bcpu.zip)
2. giải nén và đặt tại folder `<ROOT_PROJECT>/3rd_party`
```
    <ROOT_PROJECT>
    └── 3rd_party
       ├── libtorch-cxx11-abi-shared-with-deps-1.11.0+cpu.zip
       ├── libtorch-shared-with-deps-1.11.0+cpu.zip
       └── libtorch-shared-with-deps-1.11.0+cpu
           └── libtorch
               ├── bin
               ├── include
               ├── lib
               ├── share
               ├── build-hash
               └── build-version
```
3. tạo folder `<ROOT_PROJECT>/csrc`
```
mkdir csrc
```
Tất cả source code C++ đề nằm bên trong folder `csrc`   
4. Tạo file `csrc/CMakeLists.txt`

```
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(example-app)

find_package(Torch REQUIRED)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${TORCH_CXX_FLAGS}")

add_executable(example-app main.cpp) # TODO sửa tên file.cpp chứa main func
target_link_libraries(example-app "${TORCH_LIBRARIES}")
set_property(TARGET example-app PROPERTY CXX_STANDARD 14)

# The following code block is suggested to be used on Windows.
# According to https://github.com/pytorch/pytorch/issues/25457,
# the DLLs need to be copied to avoid memory errors.
if (MSVC)
  file(GLOB TORCH_DLLS "${TORCH_INSTALL_PREFIX}/lib/*.dll")
  add_custom_command(TARGET example-app
                     POST_BUILD
                     COMMAND ${CMAKE_COMMAND} -E copy_if_different
                     ${TORCH_DLLS}
                     $<TARGET_FILE_DIR:example-app>)
endif (MSVC)
```

5. Tạo file source `csrc/main.cpp`
```
#include <iostream>
#include <torch/torch.h>

int main(int argc, char* argv[]){
    torch::Tensor tensor = torch::rand({2, 3});
    std::cout << tensor << std::endl;
    return 0;
}
```

6. Cấu trúc project hiện tại (sau step 3,4 và 5)
```
    <ROOT_PROJECT>
    ├── 3rd_party
    │  ├── libtorch-cxx11-abi-shared-with-deps-1.11.0+cpu.zip
    │  ├── libtorch-shared-with-deps-1.11.0+cpu.zip
    │  └── libtorch-shared-with-deps-1.11.0+cpu
    │      └── libtorch
    │          ├── bin
    │          ├── include
    │          ├── lib
    │          ├── share
    │          ├── build-hash
    │          └── build-version
    │
    └── csrc
        ├── CMakeLists
        └── main.cpp
```

7. Tạo folder `<ROOT_PROJECT>/build` và thực hiện biên dịch chương trình
```
user@pc:<ROOT_PROJECT>$ mkdir build
user@pc:<ROOT_PROJECT>$ cd build
user@pc:<ROOT_PROJECT>/build$ cmake -DCMAKE_PREFIX_PATH=/absolute/path/to/libtorch ../csrc
user@pc:<ROOT_PROJECT>/build$ cmake --build . --config Release
user@pc:<ROOT_PROJECT>/build$ ./example-app
user@pc:<ROOT_PROJECT>/build$ cd ..
user@pc:<ROOT_PROJECT>$ 
```

```
mkdir build
cd build
cmake -DCMAKE_PREFIX_PATH=/absolute/path/to/libtorch ../csrc
cmake --build . --config Release
./example-app
cd ..
```

Nếu install pytorch bằng conda/pip, ta có thể activate môi trường python đã install pytorch, sau đó config CMake 
```
cmake -DCMAKE_PREFIX_PATH=`python -c 'import torch;print(torch.utils.cmake_prefix_path)'`
```
